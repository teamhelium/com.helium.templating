# Universal Templating Engine

Welcome to the Helium's universal template engine. This engine uses dependency injection to swap different templating engines in and out of your application.

## How To Use

The main file is the `Template` file, where the engines are the engine name file by engine. For example, `MustacheEngine.php`. Multiple engines can be implemented and swapped as such.

```
use helium\Template\Template;
use helium\Template\MustacheEngine;
use helium\Template\BladeEngine;

//These placeholders will change per engine
$string = 'Replace values {first_name} {last_name}';

//Data to replace with values
$data = ['first_name' => 'John', 'last_name' => 'Doe'];

$template = new Template(new MustacheEngine());
$template -> render($string, $data);

//This example use Blade, which requires that you have
//a writable and readable directory setup

$template = new Template(new BladeEngine('/views/users', '/tmp'));
$template->render('username', $data);
```