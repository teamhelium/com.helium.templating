<?php

namespace Helium\Template;

use Jenssegers\Blade\Blade;

/**
 * BladeEngine
 * 
 * Utilizes the blade engine for rendering templates. Please
 * refer to https://github.com/jenssegers/blade for further detailers
 */
class BladeEngine implements TemplateInterface {

    private $_bladeEngine = null;

    /**
     * Setups the blade with the required folders
     * 
     * @param string $view_folder Where your view files are located
     * @param string $cache_folder A folder to cache templates
     */
    public function __construct($view_folder, $cache_folder) {
        $this->_bladeEngine = new Blade($view_folder, $cache_folder);

    }

    public function render(string $template, array $data): string {
        return $this->_bladeEngine->make($template, $data)->render();
    }
}