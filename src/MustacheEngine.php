<?php

namespace Helium\Template;

use Mustache_Engine;

/**
 * MustacheEngine
 * 
 * Uses mustache for rendering template. Please refer to
 * https://github.com/bobthecow/mustache.php for how mustache works.
 */
class MustacheEngine implements TemplateInterface {

    private $_mustache = null;

    public function __construct() {
      $this->_mustache = new Mustache_Engine();  
    }

    public function render(string $template, array $data): string {
        return $this->_mustache->render($template, $data);
    }
}