<?php

namespace Helium\Template;

class Template implements TemplateInterface {

    private $_templateEngine = null;

    public function __construct(TemplateInterface $object) {
        $this->_templateEngine = $object;
    }

    public function render(string $template, array $data) : string {
        return $this->_templateEngine->render($template, $data);
    }

}