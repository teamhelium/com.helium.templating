<?php
namespace Helium\Template;

interface TemplateInterface {

    /**
     * Renders a template based on based in data.
     * 
     * @param string $template The template to be parsed.
     * @param array $data Data that will replace values in template
     * 
     * @return string
     */
    public function render(string $template, array $data) : string;

}